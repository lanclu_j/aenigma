package com.aenigma.games.android;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.WindowManager;

import com.aenigma.games.accelero.AccelGame;
import com.aenigma.games.audio.AudioGame;
import com.aenigma.games.compass.CompassGame;
import com.aenigma.games.touch.TouchGame;
import com.aenigma.games.vibrate.VibrateGame;
import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;

import java.io.Serializable;

public class GameLauncher extends AndroidApplication implements Serializable
{
    private ApplicationAdapter getGame(GameType type)
    {
       switch (type)
       {
           case TOUCH:
               return new TouchGame();
           case COMPASS:
               return new CompassGame();
           case VIBRATE:
               return new VibrateGame();
           case ACCELEROMETER:
               return new AccelGame();
           case AUDIO:
               return new AudioGame();
           default:
               return new TouchGame();
       }
    }

    @Override
    protected void onCreate (Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();

        // Fetch game type
        Intent intent = getIntent();
        GameType type = (GameType) intent.getSerializableExtra("gametype");

        // Set screen orientation
        if (type == GameType.TOUCH)
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        // Initialize game
        ApplicationAdapter game = getGame(type);

        // Launch game
        initialize(game, config);
    }
}
