package com.aenigma.games.android;

import android.app.Activity;
import android.content.Intent;

public class GameTools
{
    public static Intent getGameIntent(GameType gameType, Activity activity)
    {
        Intent intent = new Intent(activity, GameLauncher.class);
        intent.putExtra("gametype", gameType);
        return intent;
    }

    public static GameType getGameType(String id)
    {
        switch(id)
        {
            case "Touch":
                return GameType.TOUCH;
            case "Compass":
                return GameType.COMPASS;
            case "Vibrate":
                return GameType.VIBRATE;
            case "Accelerometer":
                return GameType.ACCELEROMETER;
            case "Audio":
                return GameType.AUDIO;
            default:
                return GameType.TOUCH;
        }
    }
}
