package com.aenigma.navigation;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.app.Fragment;
import android.support.v7.widget.CardView;
import android.text.style.TtsSpan;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.aenigma.game.android.SessionManager;
import com.aenigma.navigation.interfaces.OnFragmentInteractionListener;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.charts.RadarChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.RadarData;
import com.github.mikephil.charting.data.RadarDataSet;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Profile#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Profile extends Fragment implements ObservableScrollView.Callbacks{
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_POSITION = "position";

    private int mPosition;
    private View mHeader;
    private int maxTopHeight;
    private ObservableScrollView scroll;

    HashMap<String, String> user;
    static View view;
    static Activity activity;
    static SessionManager session;

    private OnFragmentInteractionListener mListener;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param position Parameter 1.
     * @return A new instance of fragment Profile.
     */
    public static Profile newInstance(int position) {
        Profile fragment = new Profile();
        Bundle args = new Bundle();
        args.putInt(ARG_POSITION, position);
        fragment.setArguments(args);
        return fragment;
    }

    public Profile() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        session = ((TabActivity) this.getActivity()).session;

        if (getArguments() != null) {
            mPosition = getArguments().getInt(ARG_POSITION);
        }
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             final Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_profile, container, false);
        scroll = (ObservableScrollView) view.findViewById(R.id.profilBottom);

        // Get user data from session
        user = session.getUserDetails();

        int num_connected = Integer.parseInt(user.get(SessionManager.KEY_CONNECTED));
        int lvl = Integer.parseInt(user.get(SessionManager.KEY_LEVEL));

        TextView name = (TextView) view.findViewById(R.id.profileName);
        TextView title = (TextView) view.findViewById(R.id.textView7);
        TextView connected = (TextView) view.findViewById(R.id.connected);
        TextView level = (TextView) view.findViewById(R.id.level);
        ImageView avatar = (ImageView) view.findViewById(R.id.avatar);
        ImageView hair = (ImageView) view.findViewById(R.id.hair);
        ImageView accessory = (ImageView) view.findViewById(R.id.accessory);
        Button edit = (Button) view.findViewById(R.id.editProfil);
        edit.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Starting MainActivity
                Intent i = new Intent(inflater.getContext(), EditProfilActivity.class);
                startActivity(i);
            }});
        scroll.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                onScrollChanged(scroll.getScrollY());
            }
        });

        name.setText(user.get(SessionManager.KEY_NAME));
        title.setText(getTitle());
        connected.setText(session.res.getString(R.string.connected) + " " + num_connected + " " + session.res.getString(R.string.times));
        level.setText(session.res.getString(R.string.level) + " " + lvl);
        avatar.setImageResource(session.avatar.getAvatarView());
        hair.setImageResource(session.avatar.getHairView());
        accessory.setImageResource(session.avatar.getAccessoryView());

        initChart();

        return view;
    }

    @Override
    public void onScrollChanged(int scrollY)
    {
        CardView top = (CardView) view.findViewById(R.id.profilTop);
        top.bringToFront();
        if (maxTopHeight < top.getHeight())
            maxTopHeight = top.getHeight();

        top.getLayoutParams().height = Math.max(200, maxTopHeight - scrollY);
        top.setTranslationY(Math.max(scroll.getTop(), scrollY));
        top.requestLayout();
    }

    @Override
    public void onDownMotionEvent() {
    }

    @Override
    public void onUpOrCancelMotionEvent() {
    }

    // TODO: Rename method, update argument and hook method into UI event
    /*public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
         }
    }*/

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            this.activity = activity;
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    // Update the view (should contain almost the same code as onCreateView)
    public void update() {
        // Get user data from session
        if (activity != null && view != null) {
            // Get user data from session
            user = session.getUserDetails();

            int num_connected = Integer.parseInt(user.get(session.KEY_CONNECTED));
            int lvl = Integer.parseInt(user.get(session.KEY_LEVEL));

            TextView name = (TextView) view.findViewById(R.id.profileName);
            TextView title = (TextView) view.findViewById(R.id.textView7);
            TextView connected = (TextView) view.findViewById(R.id.connected);
            TextView level = (TextView) view.findViewById(R.id.level);
            ImageView avatar = (ImageView) view.findViewById(R.id.avatar);
            ImageView hair = (ImageView) view.findViewById(R.id.hair);
            ImageView accessory = (ImageView) view.findViewById(R.id.accessory);

            name.setText(user.get(SessionManager.KEY_NAME));
            title.setText(getTitle());
            connected.setText(session.res.getString(R.string.connected) + " " + num_connected + " " + session.res.getString(R.string.times));
            level.setText(session.res.getString(R.string.level) + " " + lvl);
            avatar.setImageResource(session.avatar.getAvatarView());
            hair.setImageResource(session.avatar.getHairView());
            accessory.setImageResource(session.avatar.getAccessoryView());
        }
    }


    // Returns the title of the player as a String
    public String getTitle() {
        int level = Integer.parseInt(user.get(SessionManager.KEY_LEVEL));
        if (level >= 100)
            return session.res.getString(R.string.player_title5);
        else if (level >= 60)
            return session.res.getString(R.string.player_title4);
        else if (level >= 20)
            return session.res.getString(R.string.player_title3);
        else if (level >= 5)
            return session.res.getString(R.string.player_title2);
        else
            return session.res.getString(R.string.player_title1);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public void initChart()
    {
        PieChart chart = (PieChart) view.findViewById(R.id.chart);

        ArrayList<Entry> valsComp1 = new ArrayList<Entry>();
        int num_connected = Integer.parseInt(user.get(SessionManager.KEY_CONNECTED));
        Entry c1e1 = new Entry(num_connected, 0); // 0 == quarter 1
        valsComp1.add(c1e1);

        PieDataSet setComp1 = new PieDataSet(valsComp1, "Nombre de connection");
        setComp1.setSliceSpace(3f);

        ArrayList<String> xVals = new ArrayList<String>();
        xVals.add("");

        PieData data = new PieData(xVals, setComp1);
        chart.setData(data);
        chart.setTouchEnabled(false);
        chart.invalidate();


        // Linear Chart for show

        LineChart mLineChart = (LineChart) view.findViewById(R.id.chart2);

        ArrayList<Entry> valsLineComp1 = new ArrayList<Entry>();
        ArrayList<Entry> valsLineComp2 = new ArrayList<Entry>();

        c1e1 = new Entry(10.000f, 0); // 0 == quarter 1
        valsLineComp1.add(c1e1);
        Entry c1e2 = new Entry(5.000f, 1); // 1 == quarter 2 ...
        valsLineComp1.add(c1e2);
        // and so on ...

        Entry c2e1 = new Entry(12.000f, 0); // 0 == quarter 1
        valsLineComp2.add(c2e1);
        Entry c2e2 = new Entry(11.000f, 1); // 1 == quarter 2 ...
        valsLineComp2.add(c2e2);

        LineDataSet setLineComp1 = new LineDataSet(valsLineComp1, "Jeu 1");
        LineDataSet setLineComp2 = new LineDataSet(valsLineComp2, "Jeu 2");

        ArrayList<LineDataSet> dataSets = new ArrayList<LineDataSet>();
        dataSets.add(setLineComp1);
        dataSets.add(setLineComp2);

        ArrayList<String> xValsLine = new ArrayList<String>();
        xValsLine.add("1.Q"); xValsLine.add("2.Q"); xValsLine.add("3.Q");

        LineData dataLine = new LineData(xValsLine, dataSets);
        mLineChart.setData(dataLine);
        mLineChart.invalidate(); // refresh


        RadarChart radioChart = (RadarChart) view.findViewById(R.id.chart3);
        RadarDataSet setRadarComp1 = new RadarDataSet(valsLineComp1, "Jeu 1");
        RadarDataSet setRadarComp2 = new RadarDataSet(valsLineComp2, "Jeu 2");

        ArrayList<RadarDataSet> dataRadarSets = new ArrayList<RadarDataSet>();
        dataRadarSets.add(setRadarComp1);
        dataRadarSets.add(setRadarComp2);

        xValsLine.add("4.Q");
        xValsLine.add("5.Q");
        RadarData dataRadar = new RadarData(xValsLine, dataRadarSets);
        radioChart.setData(dataRadar);
        radioChart.invalidate();
    }
}
