package com.aenigma.navigation;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.aenigma.game.android.Avatar;
import com.aenigma.game.android.SessionManager;
import com.aenigma.navigation.interfaces.OnFragmentInteractionListener;

import java.util.HashMap;


public class EditProfilActivity extends Activity {
    HashMap<String, String> user;
    static SessionManager session;
    Avatar editAvatar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profil);
        session = new SessionManager(this);

        user = session.getUserDetails();
        editAvatar = new Avatar(session, session.avatar);

        // Get user data from session

        final EditText name = (EditText) findViewById(R.id.profilName);
        ImageView avatar = (ImageView) findViewById(R.id.imgHead);
        ImageView hair = (ImageView) findViewById(R.id.imgHair);
        ImageView acc = (ImageView) findViewById(R.id.imgAcc);
        name.setText(user.get(SessionManager.KEY_NAME));
        avatar.setImageResource(editAvatar.getAvatarView());
        hair.setImageResource(editAvatar.getHairView());
        acc.setImageResource(editAvatar.getAccessoryView());

        Button save = (Button) findViewById(R.id.edit_save_button);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                session.updateName(name.getText().toString());
                session.updatedAvatar(editAvatar);
                finish();
            }
        });

        initEditButtons();
    }

    private void initEditButtons()
    {
        initSexButtons();
        initPrevButtons();
        initNextButtons();
    }

    private void initSexButtons() {
        final Button sexH = (Button) findViewById(R.id.sexButtonH);
        final Button sexF = (Button) findViewById(R.id.sexButtonF);
        if (editAvatar.male) {
            sexH.setClickable(false);
            sexF.setClickable(true);
        }
        else
        {
            sexH.setClickable(true);
            sexF.setClickable(false);
        }
        sexH.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sexH.setClickable(false);
                sexF.setClickable(true);
                editAvatar.male = true;
                update();
            }
        });
        sexF.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sexF.setClickable(false);
                sexH.setClickable(true);
                editAvatar.male = false;
                update();
            }
        });
    }

    private void initPrevButtons() {
        Button prevHead = (Button) findViewById(R.id.headPrev);
        Button prevHair = (Button) findViewById(R.id.hairPrev);
        Button prevAcc = (Button) findViewById(R.id.accPrev);

        prevHead.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editAvatar.changeHead(-1);
                update();
            }
        });
        prevHair.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editAvatar.changeHair(-1);
                update();
            }
        });
        prevAcc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editAvatar.changeAccessory(-1);
                update();
            }
        });
    }

    private void initNextButtons() {
        Button nextHead = (Button) findViewById(R.id.headNext);
        Button nextHair = (Button) findViewById(R.id.hairNext);
        Button nextAcc = (Button) findViewById(R.id.accNext);

        nextHead.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editAvatar.changeHead(1);
                update();
            }
        });
        nextHair.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editAvatar.changeHair(1);
                update();
            }
        });
        nextAcc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editAvatar.changeAccessory(1);
                update();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_edit_profil, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    // Update the view (should contain almost the same code as onCreateView)
    public void update() {
            ImageView avatar = (ImageView) findViewById(R.id.imgHead);
            ImageView hair = (ImageView) findViewById(R.id.imgHair);
            ImageView accessory = (ImageView) findViewById(R.id.imgAcc);

            avatar.setImageResource(editAvatar.getAvatarView());
            hair.setImageResource(editAvatar.getHairView());
            accessory.setImageResource(editAvatar.getAccessoryView());
    }
}
