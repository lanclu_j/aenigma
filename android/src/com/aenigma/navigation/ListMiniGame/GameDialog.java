package com.aenigma.navigation.ListMiniGame;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.aenigma.games.android.GameType;
import com.aenigma.navigation.R;
import com.aenigma.games.android.GameTools;

public class GameDialog extends DialogFragment
{

    private GameType gameType;

    public void setGameType(GameType gameType)
    {
        this.gameType = gameType;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();

        // Prepare intent with appropriate GameType
        final Intent intent = GameTools.getGameIntent(gameType, GameDialog.this.getActivity());

        View view = inflater.inflate(R.layout.dialog_gameinfo, null);

        ((TextView) view.findViewById(R.id.dialogGameTitle)).setText(gameType.toString());

        if (gameType.toString().toLowerCase().equals("touch"))
            ((TextView) view.findViewById(R.id.dialogGameDesc)).setText(R.string.minigame1_desc);
        else if (gameType.toString().toLowerCase().equals("compass"))
            ((TextView) view.findViewById(R.id.dialogGameDesc)).setText(R.string.minigame2_desc);
        else if (gameType.toString().toLowerCase().equals("vibrate"))
            ((TextView) view.findViewById(R.id.dialogGameDesc)).setText(R.string.minigame3_desc);
        else if (gameType.toString().toLowerCase().equals("accelerometer"))
            ((TextView) view.findViewById(R.id.dialogGameDesc)).setText(R.string.minigame4_desc);
        else if (gameType.toString().toLowerCase().equals("audio"))
            ((TextView) view.findViewById(R.id.dialogGameDesc)).setText(R.string.minigame5_desc);
        else
            ((TextView) view.findViewById(R.id.dialogGameDesc)).setText("No description found.");

        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        builder.setView(view)
                // Add action buttons
                .setPositiveButton(R.string.launch, new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        startActivity(intent);
                    }
                })
                .setNegativeButton(R.string.back, new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface dialog, int id)
                    {
                        GameDialog.this.getDialog().cancel();
                    }
                });
        return builder.create();
    }
}
