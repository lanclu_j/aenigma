package com.aenigma.game.android;

import android.util.Log;
import android.widget.TableLayout;

import com.aenigma.navigation.R;
import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.audio.AudioRecorder;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class Controller implements ApplicationListener {

    public TableLayout layout;
    public AndroidLauncher view;
    public AudioRecorder mic;
    public short[] sample;

    SpriteBatch batch;
    Texture texture;
    Sprite sprite;
    float speedX = 0;
    float speedY = 0;
    float maxSpeed = 10;

    @Override
    public void create() {

        batch = new SpriteBatch();
        texture = new Texture("ball.png");
        sprite = new Sprite(texture);

        boolean available = Gdx.input.isPeripheralAvailable(Input.Peripheral.Compass);
        Log.e("ACCELEROMETER", available + "");
        available = Gdx.input.isPeripheralAvailable(Input.Peripheral.Accelerometer);
        Log.e("COMPASS", available + "");
        available = Gdx.input.isPeripheralAvailable(Input.Peripheral.MultitouchScreen);
        Log.e("MULTITOUCH", available + "");
        available = Gdx.input.isPeripheralAvailable(Input.Peripheral.Vibrator);
        Log.e("VIBRATION", available + "");

        mic = Gdx.audio.newAudioRecorder(22050, true);
        sample = new short[1024]; // 1024 samples
        mic.read(sample, 0, sample.length);

        Gdx.graphics.setDisplayMode(90, 160, true);
    }

    public void init(AndroidLauncher app) {
        view = app;
    }

    public void updateAccel() {

        int accelX = (int)Gdx.input.getAccelerometerX();
        int accelY = (int)Gdx.input.getAccelerometerY();
        int accelZ = (int)Gdx.input.getAccelerometerZ();

        String stats = accelX + ", " + accelY + ", " + accelZ;

        if (speedX < maxSpeed)
            speedX = speedX + (accelX > 0 ? 0.1f: -0.1f);

        if (speedY < maxSpeed)
            speedY = speedY + (accelY > 0 ? 0.1f: -0.1f);

        view.updateAccel(stats);
    }

    public void updateTouch() {

        java.lang.String touch, coord;
        if (Gdx.input.isTouched()) {
            touch = view.getString(R.string.touched);
            coord = Gdx.input.getX() + ", " + Gdx.input.getY();
        }
        else {
            touch = view.getString(R.string.untouched);
            coord = "-";
        }
        view.updateTouch(touch, coord);
    }

    public void updateGyro() {

        int gyroX = (int)Gdx.input.getPitch();
        int gyroY = (int)Gdx.input.getRoll();
        int gyroZ = (int)Gdx.input.getAzimuth();

        java.lang.String stats = gyroX + ", " + gyroY + ", " + gyroZ;

        view.updateGyro(stats);
    }

    @Override
    public void resize(int width, int height) {

    }

    public void render() {
        Gdx.gl.glClearColor(1, 1, 1, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        batch.begin();
        sprite.draw(batch);
        batch.end();

        updateGyro();
        updateTouch();
        updateAccel();

        moveSprite();
    }

    public void moveSprite() {
        if (sprite.getY() > Gdx.graphics.getHeight() - sprite.getHeight() || sprite.getY() < 0) {
            speedY = 0;
            if (sprite.getY() < 0)
                sprite.setPosition(sprite.getX(), 0);
            else
                sprite.setPosition(sprite.getX(), Gdx.graphics.getHeight() - sprite.getHeight());
        }
        if (sprite.getX() < 0 || sprite.getX() > Gdx.graphics.getWidth() - sprite.getWidth()) {
            speedX = 0;
            if (sprite.getX() < 0)
                sprite.setPosition(0, sprite.getY());
            else
                sprite.setPosition(Gdx.graphics.getWidth() - sprite.getWidth(), sprite.getY());
        }
        sprite.setPosition(sprite.getX() - 1 * speedX, sprite.getY() - 1 * speedY);
    }

    public int recordMic() {
        mic.read(sample, 0, sample.length);
        return sample[0];
    }
    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void dispose() {
        batch.dispose();
        texture.dispose();
        mic.dispose();
    }
}
