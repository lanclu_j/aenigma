package com.aenigma.game.android;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.View;
import android.widget.TableLayout;

import com.aenigma.navigation.R;
import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.audio.AudioRecorder;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

enum TrainStates
{
    NORMAL, ACCEL, GO, BRAKE, ACCIDENT, END
};

public class ArcadeAnim implements ApplicationListener {

    public TableLayout layout;
    public Arcade view;
    Drawable background;
    float elapsedTime = 0;

    public int width, height;

    SpriteBatch batch;
    TextureAtlas texture;
    Animation animation;

    TrainStates state = TrainStates.NORMAL;

    int offset = 0;
    float scale, scaleX, scaleY;

    public ArcadeAnim(int width, int height) {
        this.width = width;
        this.height = height;
        this.scale = height * 1.5f / 480;
        this.scaleY = scale;
        this.scaleX = scale;
    }

    @Override
    public void create() {
        batch = new SpriteBatch();
        texture = new TextureAtlas("train.atlas");
        animation = new Animation(1/8f, texture.getRegions());

        Gdx.graphics.setDisplayMode(width, height, true);
    }

    public void init(Arcade view, Drawable background) {
        this.view = view;
        this.background = background;
    }

    @Override
    public void resize(int width, int height) {
    }

    public void render() {
        if (background != null) {
            int color = ((ColorDrawable) background).getColor();
            Gdx.gl.glClearColor(Color.red(color), Color.green(color), Color.blue(color), Color.alpha(color));
        }

        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        batch.begin();

        elapsedTime += Gdx.graphics.getDeltaTime();
        TextureRegion anim = animation.getKeyFrame(elapsedTime, true);

        if (state == TrainStates.BRAKE || state == TrainStates.ACCIDENT || state == TrainStates.END) {
            scaleX = scale;
            offset -= 10;
            if (state != TrainStates.BRAKE) {
                scaleY = scale / 1.875f;
                System.out.println("BRAKING + " + scaleX);
            }
        }
        else if (state == TrainStates.GO) {
            scaleX -= scale / 75f;

            if (scaleX < scale / 1.875f)
                state = TrainStates.ACCEL;
        }
        else if (state == TrainStates.ACCEL) {
            if (scaleX < scale / 1.875f)
                scaleX += scale / 37.5f;
            offset += 30;
        }

        batch.draw(anim, offset, 0, anim.getRegionWidth() * scaleX, anim.getRegionHeight() * scaleY);

        if (state != TrainStates.END) {
            if (offset > width) {
                state = TrainStates.NORMAL;
                offset = width;
                view.startGame();
            } else if (offset < 0) {
                state = TrainStates.NORMAL;
                scaleX = scale;
                scaleY = scale;
                offset = 0;
            }
        }
        else {
            if (offset < -width)
                view.finish();
        }

        batch.end();
    }

    @Override
    public void pause() {
    }

    @Override
    public void resume() {
    }

    @Override
    public void dispose() {
        batch.dispose();
        texture.dispose();
    }
}

