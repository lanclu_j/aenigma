package com.aenigma.game.android;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListAdapter;

import com.aenigma.navigation.ListMiniGame.GameDialog;
import com.aenigma.navigation.ListMiniGame.MiniGameAdapter;
import com.aenigma.navigation.ListMiniGame.MiniGameListContent;
import com.aenigma.navigation.R;
import com.aenigma.games.android.GameTools;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Large screen devices (such as tablets) are supported by replacing the ListView
 * with a GridView.
 * <p/>
 * Activities containing this fragment MUST implement the {@link com.aenigma.navigation.interfaces.OnFragmentInteractionListener}
 * interface.
 */
public class GamesList extends Activity implements AbsListView.OnItemClickListener
{
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_POSITION = "position";

    private int mPosition;

    /**
     * The fragment's ListView/GridView.
     */
    private AbsListView mListView;

    /**
     * The Adapter which will be used to populate the ListView/GridView with
     * Views.
     */
    private ListAdapter mAdapter;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public GamesList()
    {
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        // TODO: Change Adapter to display your content
        mAdapter = new MiniGameAdapter(this, MiniGameListContent.ITEMS);

        setContentView(R.layout.fragment_itemgame_grid);

        // Set the adapter
        View view = findViewById(android.R.id.content);

        mListView = (AbsListView) view.findViewById(android.R.id.list);
        ((AdapterView<ListAdapter>) mListView).setAdapter(mAdapter);

        // Set OnItemClickListener so we can be notified on item clicks
        mListView.setOnItemClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id)
    {
        GameDialog dialog = new GameDialog();
        dialog.setGameType(GameTools.getGameType(MiniGameListContent.ITEMS.get(position).id));

        dialog.show(getFragmentManager(), "Game info");
    }
}
