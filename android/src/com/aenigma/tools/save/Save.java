package com.aenigma.tools.save;
import android.app.Dialog;
import android.os.Bundle;
import android.util.Log;

import com.aenigma.game.android.SessionManager;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.games.Games;
import com.google.android.gms.games.GamesStatusCodes;
import com.google.android.gms.games.snapshot.Snapshot;
import com.google.android.gms.games.snapshot.SnapshotMetadataChange;
import com.google.android.gms.games.snapshot.Snapshots;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Calendar;
import java.util.Map;

public class Save implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    // Private Save used for the singleton design pattern
    private final static Save INSTANCE = new Save();

    // On the first save, the Snapshot have to be created
    private boolean firstSave = true;

    // Tmp data to be used when connection to Google' servers op
    private Map mDataToSave;

    // Used when connection to Google' servers op
    private Map mDataLoaded;

    // Tmp dialog to be used when connection to Google' servers op
    private Dialog mDialog;

    // Indicate if trying to save/load
    private boolean trySaving = false;
    private boolean tryLoading = false;

    // Save name
    private final String mCurrentSaveName = "snapshot-aenigma";

    // Private constructor for the singleton design pattern
    private Save() {
    }

    // Return a single instance of Save. Thanks to the Singleton design pattern.
    public static Save getInstance() {
        return INSTANCE;
    }

    // Return the loaded Data form the save
    public Map getmDataLoaded() {
        return mDataLoaded;
    }

    /**
     * Prepares saving Snapshot to the user's synchronized storage, conditionally resolves errors,
     * and stores the Snapshot.
     *
     * @param data The map to save
     */
    public void saveSnapshot(final Map data) {
        final GoogleApiClient googleApiClient = SessionManager.getmGoogleApiClient();

        synchronized (googleApiClient) {
            if (googleApiClient.isConnected()) {
                mDataToSave = data;
                Games.Snapshots.open(googleApiClient, mCurrentSaveName, firstSave)
                        .setResultCallback(new ResultCallback<Snapshots.OpenSnapshotResult>() {
                            @Override
                            public void onResult(Snapshots.OpenSnapshotResult openSnapshotResult) {
                                Snapshot toWrite = processSnapshotOpenResult(googleApiClient, openSnapshotResult, 5);
                                writeSnapshot(googleApiClient, toWrite, mDataToSave);
                                firstSave = false;

                                if (trySaving) {
                                    SessionManager.getmGoogleApiClient().disconnect();
                                    trySaving = false;
                                }
                            }
                        });

            } else {
                if (!trySaving)
                    mDataToSave = data;
                trySaving = true;
                SessionManager.getmGoogleApiClient().connect();
            }
        }
    }

    /**
     * Conflict resolution for when Snapshots are opened.
     *
     * @param result The open snapshot result to resolve on open.
     * @return The opened Snapshot on success; otherwise, returns null.
     */
    private Snapshot processSnapshotOpenResult(final GoogleApiClient googleApiClient, final Snapshots.OpenSnapshotResult result, final int retryCount) {
        Snapshot mResolvedSnapshot = null;

        int status = result.getStatus().getStatusCode();

        if (status == GamesStatusCodes.STATUS_OK) {
            return result.getSnapshot();
        } else if (status == GamesStatusCodes.STATUS_SNAPSHOT_CONTENTS_UNAVAILABLE) {
            return result.getSnapshot();
        } else if (status == GamesStatusCodes.STATUS_SNAPSHOT_CONFLICT) {
            Snapshot snapshot = result.getSnapshot();
            Snapshot conflictSnapshot = result.getConflictingSnapshot();

            // Resolve between conflicts by selecting the newest of the conflicting snapshots.
            mResolvedSnapshot = snapshot;

            if (snapshot.getMetadata().getLastModifiedTimestamp() <
                    conflictSnapshot.getMetadata().getLastModifiedTimestamp()) {
                mResolvedSnapshot = conflictSnapshot;
                Games.Snapshots.delete(googleApiClient, snapshot.getMetadata());
            }

            Games.Snapshots.resolveConflict(
                    googleApiClient, result.getConflictId(), mResolvedSnapshot)
                    .setResultCallback(new ResultCallback<Snapshots.OpenSnapshotResult>() {
                        @Override
                        public void onResult(Snapshots.OpenSnapshotResult openSnapshotResult) {
                            if (retryCount < 10) {
                                processSnapshotOpenResult(googleApiClient, openSnapshotResult, retryCount + 1);
                            }
                            else {
                                String message = "Could not resolve snapshot conflicts";
                                Log.e("aenigma - snapshot", message);
                            }
                        }
                    });
        }
        // Fail, return null.
        return null;
    }

    /**
     * Generates metadata, takes a screenshot, and performs the write operation for saving a
     * snapshot.
     */
    private void writeSnapshot(GoogleApiClient mGoogleApiClient, Snapshot snapshot, Map data) {

        byte[] dataToSave = null;
        if (data != null) {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();

            try {
                ObjectOutputStream oos = new ObjectOutputStream(baos);
                oos.writeObject(data);
                oos.flush();
                oos.close();
                dataToSave = baos.toByteArray();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
        else {
            dataToSave = new byte[0];
        }

        snapshot.getSnapshotContents().writeBytes(dataToSave);

        // Save the snapshot.
        SnapshotMetadataChange metadataChange = new SnapshotMetadataChange.Builder()
                .setDescription(Calendar.getInstance().getTime().toString())
                .build();
        Games.Snapshots.commitAndClose(mGoogleApiClient, snapshot, metadataChange);
    }

    /** Set Map of saved data from the unique save
     *
     * @param dialog waiting dialog which will be dissmiss at the end
     */

    public void loadData(Dialog dialog) {
        if (SessionManager.getmGoogleApiClient().isConnected()) {

            final GoogleApiClient googleApiClient = SessionManager.getmGoogleApiClient();
            mDialog = dialog;
            Games.Snapshots.open(googleApiClient, mCurrentSaveName, false)
                .setResultCallback(new ResultCallback<Snapshots.OpenSnapshotResult>() {
                        @Override
                        public void onResult(Snapshots.OpenSnapshotResult openSnapshotResult) {
                            Snapshot toWrite = processSnapshotOpenResult(googleApiClient, openSnapshotResult, 5);

                            try {
                                byte[] data = toWrite.getSnapshotContents().readFully();

                                if (data == null)
                                    return;

                                firstSave = false;
                                ByteArrayInputStream bais = new ByteArrayInputStream(data);
                                ObjectInputStream ois = new ObjectInputStream(bais);
                                mDataLoaded = (Map)ois.readObject();

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            finally {
                                mDialog.dismiss();
                                SessionManager.getmGoogleApiClient().disconnect();
                                tryLoading = false;
                            }
                        }
                });

        } else {
            if (!tryLoading)
                tryLoading = true;
            mDialog = dialog;
            SessionManager.getmGoogleApiClient().connect();
        }

    }

    @Override
    public void onConnected(Bundle bundle) {
        if (tryLoading) {
            this.loadData(mDialog);
        }
        if (trySaving) {
            this.saveSnapshot(mDataToSave);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        trySaving = false;
        tryLoading = false;
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        trySaving = false;
        tryLoading = false;
    }
}
