package com.aenigma.tools;

import android.content.Context;
import android.graphics.Bitmap;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.games.Games;
import com.google.android.gms.games.snapshot.Snapshot;
import com.google.android.gms.games.snapshot.SnapshotMetadataChange;
import com.google.android.gms.games.snapshot.Snapshots;

import java.lang.reflect.Field;

public class Tools {
    public static int getId(String name, Class<?> c) {
        int id = -1;
        try {
            Field field = c.getField(name);
            id = field.getInt(null);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return id;
    }

    public static String getResource(Context context, int id) {
        return context.getResources().getString(id);
    }


}
