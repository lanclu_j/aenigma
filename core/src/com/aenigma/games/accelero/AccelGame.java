package com.aenigma.games.accelero;

import com.aenigma.games.GameAuditable;
import com.aenigma.games.GameState;
import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;

import java.util.Date;
import java.util.Dictionary;

public class AccelGame extends ApplicationAdapter implements GameAuditable
{
    private AccelView accelView;
    private AccelLogic accelLogic;

    private Date startDate;
    private Date endDate;
    private Dictionary<String, Float> parameters;
    private Dictionary<String, Float> values;

    @Override
    public void create()
    {
        accelView = new AccelView();
        accelLogic = new AccelLogic();

        startDate = new Date();
    }

    @Override
    public void resize(int width, int height)
    {
        accelView.updateViewport(width, height);
        accelLogic.setGame(width, height);
        startDate = new Date();
    }

    @Override
    public void render()
    {
        accelLogic.update();
        accelView.render(accelLogic);

        if (accelLogic.getState() == GameState.ENDED)
        {
            endDate = new Date();
            Gdx.app.exit();
        }
    }

    private void initializeParameters()
    {
       // FIXME
    }

    private void initializeValues()
    {
        // FIXME
    }

    @Override
    public Float getScore()
    {
        return null;
    }

    @Override
    public Float getSuccessRate()
    {
        return null;
    }

    @Override
    public Date getStartTime()
    {
        return null;
    }

    @Override
    public Date getEndTime()
    {
        return null;
    }

    @Override
    public Float getParameter(String name)
    {
        return null;
    }

    @Override
    public Float getValue(String name)
    {
        return null;
    }
}
