package com.aenigma.games.accelero.model;

import com.badlogic.gdx.math.Vector2;

public class Player extends Element
{
    private final float INITIAL_VELOCITY = 1f;
    private final float ACCELERATION = 0.2f;

    public Player(Vector2 position, int size)
    {
        super(position, size, ElementType.PLAYER);
        velocity = INITIAL_VELOCITY;
    }

    public void update(float oldAccelX, float accelX, float worldWidth)
    {
        // Update velocity
        if ((accelX > 0 && oldAccelX < 0) || (accelX < 0 && oldAccelX > 0))
            velocity = INITIAL_VELOCITY;
        else
            velocity += ACCELERATION;

        // Update position
        position.x -= accelX * velocity;

        // Check bounds
        if (position.x < -worldWidth / 2)
        {
            position.x = -worldWidth / 2;
            velocity = INITIAL_VELOCITY;
        }
        else if (position.x + size >= worldWidth / 2)
        {
            position.x = worldWidth / 2 - size;
            velocity = INITIAL_VELOCITY;
        }
    }
}
