package com.aenigma.games;

import java.util.Date;

public interface GameAuditable
{
    public Float getScore();
    public Float getSuccessRate();

    public Date getStartTime();
    public Date getEndTime();

    public Float getParameter(final String name);
    public Float getValue(final String name);
}
