package com.aenigma.games.compass;

import com.aenigma.games.GameState;
import com.aenigma.games.compass.model.CardinalPoint;
import com.aenigma.games.compass.model.Timer;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;

import java.util.ArrayList;
import java.util.Random;

public class CompassLogic
{
    private GameState state;

    private CardinalPoint currentDirection;
    private ArrayList<CardinalPoint> sequence;
    private final int SEQUENCE_SIZE = 10;

    private Timer timer;
    private final int REACTION_TIME = 5000;

    final int TARGET_RANGE = 30;
    private Float score;
    private Float scoreOutOf;

    public CompassLogic()
    {
        state = GameState.INITIALIZING;
    }

    public void setGame()
    {
        sequence = generateSequence(SEQUENCE_SIZE);
        timer = new Timer(REACTION_TIME);
        score = 0f;
        scoreOutOf = (float) SEQUENCE_SIZE;

        // Launch game
        timer.start();
        state = GameState.ONGOING;
    }

    public void update()
    {
        if (sequence.isEmpty())
            state = GameState.ENDED;

        double elapsedTime = timer.getElapsedTime();

        // Launch next round
        if (elapsedTime > REACTION_TIME)
        {
            // Reset timer
            timer.start();
            elapsedTime = 0;

            // Goes to next element
            if (!sequence.isEmpty())
                sequence.remove(0);
        }


        // Determine current direction
        float azimuth = Gdx.input.getAzimuth();
        currentDirection = getCP(azimuth);
    }

    // Tools

    private CardinalPoint getCP(float azimuth)
    {
        final int BOUND_N = TARGET_RANGE;
        final int BOUND_E = 90;
        final int BOUND_S = 180;
        final int BOUND_W = -90;
        final int BOUND_NE = BOUND_E - TARGET_RANGE;
        final int BOUND_SE = BOUND_E + TARGET_RANGE;
        final int BOUND_NW = -BOUND_NE;
        final int BOUND_SW = -BOUND_SE;

        if (azimuth > 0)
        {
            if (azimuth < BOUND_N)
                return CardinalPoint.NORTH;
            else if (azimuth < BOUND_NE)
                return CardinalPoint.NORTH_EAST;
            else if (azimuth < BOUND_E)
                return CardinalPoint.EAST;
            else if (azimuth < BOUND_SE)
                return CardinalPoint.EAST;
            else if (azimuth < BOUND_S)
                return CardinalPoint.SOUTH_EAST;
            else
                return CardinalPoint.SOUTH;
        }
        else
        {
             if (azimuth > -BOUND_N)
                return CardinalPoint.NORTH;
            else if (azimuth > BOUND_NW)
                return CardinalPoint.NORTH_WEST;
            else if (azimuth > BOUND_W)
                return CardinalPoint.WEST;
            else if (azimuth > BOUND_SW)
                return CardinalPoint.WEST;
            else if (azimuth > BOUND_S)
                return CardinalPoint.SOUTH_WEST;
            else
                return CardinalPoint.SOUTH;
        }
    }

    private ArrayList<CardinalPoint> generateSequence(int n)
    {
        ArrayList<CardinalPoint> out = new ArrayList<CardinalPoint>();

        for (int i = 0; i < n; i++)
        {
            Random rand = new Random();
            int nb = rand.nextInt(4) + 1;

            CardinalPoint newDirection = CardinalPoint.EAST;

            switch (nb)
            {
                case 1:
                    newDirection = CardinalPoint.NORTH;
                    break;
                case 2:
                    newDirection = CardinalPoint.WEST;
                    break;
                case 3:
                    newDirection = CardinalPoint.EAST;
                    break;
                case 4:
                    newDirection = CardinalPoint.SOUTH;
                    break;
            }

            if (i == 0 || newDirection != out.get(i - 1))
                out.add(newDirection);
            else
                i--;
        }

        return out;
    }

    // Accessors & Mutators
    public float getTimerSize(final float commonSize, double elapsedTime)
    {
         if (state == GameState.ONGOING && !sequence.isEmpty())
            return commonSize * ((float) elapsedTime) / REACTION_TIME;
        else
            return commonSize;

    }

    public double getElapsedTime()
    {
        return timer.getElapsedTime();
    }

    public boolean isSequenceEmpty()
    {
        return sequence.isEmpty();
    }

    public void tapping()
    {
        if (state == GameState.ONGOING && !sequence.isEmpty()
                && currentDirection == getTargetCP())
        {
            timer.start();
            sequence.remove(0);
            score++;

            if (sequence.isEmpty())
                state = GameState.ENDED;
        }
    }

    public int getSequenceSize()
    {
        return SEQUENCE_SIZE;
    }

    public Float getScore()
    {
        return score;
    }

    public Float getSuccessRate()
    {
        return score / scoreOutOf;
    }

    public GameState getState()
    {
        return state;
    }

    public CardinalPoint getTargetCP()
    {
        return sequence.get(0);
    }

    public CardinalPoint getCurrentCP()
    {
        return currentDirection;
    }
}
