package com.aenigma.games.compass.model;

import java.util.Date;

public class Timer
{
    private long startTime;
    private long maxTime;

    public Timer(long maxTime)
    {
        this.maxTime = maxTime;
    }

    public void start()
    {
        startTime = System.currentTimeMillis();
    }

    public long getElapsedTime()
    {
        long now = System.currentTimeMillis();
        return now - startTime;
    }
}
