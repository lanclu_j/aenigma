package com.aenigma.games.compass.model;

public enum CardinalPoint
{
    NORTH,
    NORTH_EAST,
    NORTH_WEST,
    EAST,
    WEST,
    SOUTH_EAST,
    SOUTH_WEST,
    SOUTH
}
