package com.aenigma.games.compass;

import com.aenigma.games.GameAuditable;
import com.aenigma.games.GameState;
import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.input.GestureDetector;

import java.util.Date;
import java.util.Dictionary;

public class CompassGame extends ApplicationAdapter implements GameAuditable
{
    private CompassView compassView;
    private CompassLogic compassLogic;

    private Date startDate;
    private Date endDate;
    private Dictionary<String, Float> parameters;
    private Dictionary<String, Float> values;

    private GestureDetector detector;

    // ApplicationAdapater overrides

    @Override
    public void create()
    {
        // Initialize scene and logic
        compassLogic = new CompassLogic();
        compassView = new CompassView();

        // Attach sensor detector
        detector = new GestureDetector(new CompassGestureListener(compassLogic));
        Gdx.input.setInputProcessor(detector);

        // Start game
        startDate = new Date();
    }

    private void initializeParameters()
    {
       // FIXME
    }

    private void initializeValues()
    {
        // FIXME
    }

    @Override
    public void resize(int width, int height)
    {
        compassView.updateViewport(width, height);

        if (compassLogic.getState() == GameState.INITIALIZING)
            compassLogic.setGame();
    }

    @Override
    public void render()
    {
        compassLogic.update();
        compassView.render(compassLogic);

        if (compassLogic.getState() == GameState.ENDED)
        {
            endDate = new Date();
            Gdx.app.exit();
        }
    }

    // GameAuditable implementation

    @Override
    public Float getScore()
    {
        return compassLogic.getScore();
    }

    @Override
    public Float getSuccessRate()
    {
        return compassLogic.getSuccessRate();
    }

    @Override
    public Date getStartTime()
    {
        return startDate;
    }

    @Override
    public Date getEndTime()
    {
        return endDate;
    }

    @Override
    public Float getParameter(String name)
    {
        return parameters.get(name);
    }

    @Override
    public Float getValue(String name)
    {
        return values.get(name);
    }
}
