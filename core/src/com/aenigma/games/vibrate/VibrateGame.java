package com.aenigma.games.vibrate;

import com.aenigma.games.GameAuditable;
import com.aenigma.games.GameState;
import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.input.GestureDetector;

import java.util.Date;
import java.util.Dictionary;

public class VibrateGame extends ApplicationAdapter implements GameAuditable
{
    private VibrateView vibrateView;
    private VibrateLogic vibrateLogic;

    private Date startDate;
    private Date endDate;
    private Dictionary<String, Float> parameters;
    private Dictionary<String, Float> values;

    private VibrateGestureListener listener;

    // ApplicationAdapater overrides

    @Override
    public void create()
    {
        // Initialize scene and logic
        vibrateLogic = new VibrateLogic();
        vibrateView = new VibrateView();

        // Attach sensor detector
        listener = new VibrateGestureListener(vibrateLogic);
        GestureDetector detector = new GestureDetector(listener);
        Gdx.input.setInputProcessor(detector);

        // Start game
        startDate = new Date();
    }

    private void initializeParameters()
    {
       // FIXME
    }

    private void initializeValues()
    {
        // FIXME
    }

    @Override
    public void resize(int width, int height)
    {
        vibrateView.updateViewport(width, height);

        if (vibrateLogic.getState() == GameState.INITIALIZING)
            vibrateLogic.setGame(width, height);

        vibrateView.initButtons();
        listener.setButtons(vibrateView.getButtons());
    }

    @Override
    public void render()
    {
        vibrateView.render(vibrateLogic);

        if (vibrateLogic.getState() == GameState.ENDED)
        {
            endDate = new Date();
            Gdx.app.exit();
        }
    }

    // GameAuditable implementation

    @Override
    public Float getScore()
    {
        return vibrateLogic.getScore();
    }

    @Override
    public Float getSuccessRate()
    {
        return 0f; // FIXME
    }

    @Override
    public Date getStartTime()
    {
        return startDate;
    }

    @Override
    public Date getEndTime()
    {
        return endDate;
    }

    @Override
    public Float getParameter(String name)
    {
        return parameters.get(name);
    }

    @Override
    public Float getValue(String name)
    {
        return values.get(name);
    }
}
