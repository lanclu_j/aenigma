package com.aenigma.games.vibrate;

import com.aenigma.games.GameScene;
import com.aenigma.games.GameState;
import com.aenigma.games.Utils;
import com.aenigma.games.vibrate.model.Button;
import com.aenigma.games.vibrate.model.Display;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;

import java.util.ArrayList;

public class VibrateView extends GameScene
{
    private BitmapFont font;
    private BitmapFont fontDisplay;
    private BitmapFont fontDisplayDenied;
    private BitmapFont fontDisplayGranted;

    private ArrayList<Button> buttons;
    private Display display;

    private final Color BACKGROUND = Color.valueOf("0e0f12");
    private final Color DIGITS_FG = Color.valueOf("2400ff");
    private final Color DIGITS_BG = Color.valueOf("16171b");
    private final Color RESET_FG = Color.valueOf("a7a7a7");
    private final Color RESET_BG = Color.valueOf("220b09");
    private final Color VALID_FG = Color.valueOf("a7a7a7");
    private final Color VALID_BG = Color.valueOf("121c12");
    private final Color DISPLAY_FG_DENIED = Color.valueOf("c70c0c");
    private final Color DISPLAY_FG_GRANTED = Color.valueOf("0add00");
    private final Color DISPLAY_BG = Color.valueOf("0b0b0d");

    public VibrateView()
    {
        font = Utils.generateFont("fonts/Square.ttf", 150, Color.LIGHT_GRAY);
        fontDisplay = Utils.generateFont("fonts/Square.ttf", 150, Color.BLUE);
        fontDisplayDenied = Utils.generateFont("fonts/Square.ttf", 80, DISPLAY_FG_DENIED);
        fontDisplayGranted = Utils.generateFont("fonts/Square.ttf", 80, DISPLAY_FG_GRANTED);
    }

    public void initButtons()
    {
        String[][] numbers = new String[][]{
                {"1", "2", "3"},
                {"4", "5", "6"},
                {"7", "8", "9"},
                {"r", "0", "v"}
        };

        ArrayList<Button> result = new ArrayList<Button>();

        float panelWidth = (int) viewport.getWorldWidth() * 75 / 100;
        float buttonSpacer = 20;
        float commonHeight = (panelWidth - 2 * buttonSpacer) / 3;

        float panelHeight = 5 * commonHeight + 5 * buttonSpacer;
        float marginWidth = (viewport.getWorldWidth() - panelWidth) / 2;
        float marginHeight = (viewport.getWorldHeight() - panelHeight) / 2;

        display = new Display(-viewport.getWorldWidth() / 2 + marginWidth,
                              viewport.getWorldHeight() / 2 - marginHeight - commonHeight,
                              panelWidth, commonHeight);

        float BUTTON_TOP = viewport.getWorldHeight() / 2 - marginHeight - commonHeight - 2 * buttonSpacer;

        for (int i = 0; i < 4; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                float x = -viewport.getWorldWidth() / 2 + marginWidth + j * (commonHeight + buttonSpacer);
                float y = BUTTON_TOP - i * (commonHeight + buttonSpacer);
                Button b = new Button(numbers[i][j], new Vector2(x, y), commonHeight);

                float fontWidth = font.getBounds(numbers[i][j]).width;
                float fontHeight = font.getBounds(numbers[i][j]).height;
                float fontX = x + (commonHeight - fontWidth) / 2;
                float fontY = y - (commonHeight - fontHeight) / 2;

                b.setTextPosition(new Vector2(fontX, fontY));

                result.add(b);
            }
        }

        buttons = result;
    }

    public void render(VibrateLogic logic)
    {
        Gdx.gl.glClearColor(BACKGROUND.r, BACKGROUND.g, BACKGROUND.b, BACKGROUND.a);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        updateComponents();

        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);

        // Draw buttons
        for (Button b : buttons)
        {
            if (b.getText().equals("v"))
                shapeRenderer.setColor(VALID_BG);
            else if (b.getText().equals("r"))
                shapeRenderer.setColor(RESET_BG);
            else
                shapeRenderer.setColor(DIGITS_BG);

            shapeRenderer.rect(b.getPosition().x, b.getPosition().y, b.getSize(), -b.getSize());
        }

        // Draw display
        shapeRenderer.setColor(DISPLAY_BG);
        shapeRenderer.rect(display.getPosition().x, display.getPosition().y, display.getWidth(), display.getHeight());

        shapeRenderer.end();

        spriteBatch.begin();

        // Writing buttons text
        for (Button b : buttons)
            font.draw(spriteBatch, b.getText(), b.getTextPosition().x, b.getTextPosition().y);



        if (logic.getState() == GameState.ENDED)
        {
            String text = "access granted";
            float textWidth = fontDisplayGranted.getBounds(text).width;
            float textHeight = fontDisplayGranted.getBounds(text).height;
            float textX = display.getPosition().x  + (display.getWidth() - textWidth) / 2;
            float textY = display.getPosition().y + (display.getHeight() / 2) + textHeight;
            fontDisplayGranted.draw(spriteBatch, text, textX, textY - textHeight / 2);
        }
        else if (logic.getState() == GameState.FEEDBACK)
        {
            String text = "access denied";
            float textWidth = fontDisplayDenied.getBounds(text).width;
            float textHeight = fontDisplayDenied.getBounds(text).height;
            float textX = display.getPosition().x  + (display.getWidth() - textWidth) / 2;
            float textY = display.getPosition().y + (display.getHeight() / 2) + textHeight;
            fontDisplayDenied.draw(spriteBatch, text, textX, textY - textHeight / 2);
        }
        else
        {
            String text = logic.getCurrentCode();
            float textHeight = font.getBounds(text).height;
            float textX = display.getPosition().x + 80;
            float textY = display.getPosition().y + (display.getHeight() - textHeight) / 2 + textHeight;
            fontDisplay.draw(spriteBatch, text, textX, textY);
        }

        spriteBatch.end();
    }

    public ArrayList<Button> getButtons()
    {
        return buttons;
    }
}
