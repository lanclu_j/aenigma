package com.aenigma.games.vibrate;

import com.aenigma.games.GameState;
import com.aenigma.games.vibrate.model.Button;
import com.aenigma.games.vibrate.model.Digit;
import com.badlogic.gdx.math.Vector2;

import java.util.ArrayList;
import java.util.Random;

public class VibrateLogic
{
    private GameState state;
    private float worldWidth;
    private float worldHeight;
    private float score;
    private final int CODE_LENGTH = 3;

    private String code;
    private String currentCode;
    private ArrayList<Digit> digits;

    public VibrateLogic()
    {
        state = GameState.INITIALIZING;
    }

    public void setGame(int width, int height)
    {
        worldWidth = width;
        worldHeight = height;
        state = GameState.ONGOING;
        code = generateCode();
        currentCode = "";
        digits = generateDigits();
    }

    private String generateCode()
        {
        Random rand = new Random();
        int value = rand.nextInt((int) Math.pow(10, CODE_LENGTH - 1));

        if (value < Math.pow(10, CODE_LENGTH - 1))
        {
            String part2 = String.valueOf(value);
            while (part2.length() < CODE_LENGTH)
                part2 = "0" + part2;
            return part2;
        }

        return String.valueOf(value);
    }

    private ArrayList<Digit> generateDigits()
    {
        ArrayList<Digit> result = new ArrayList<Digit>();

        for (int i = 0; i < 10; i++)
            result.add(new Digit(i));

        return result;
    }

    private boolean isInCode(int i)
    {
        return code.contains(String.valueOf(i));
    }

    private int isNeighbor(int i)
    {
        int counter = 0;

        for (Digit d : digits)
        {
            if (d.getValue() != i && isInCode(d.getValue()) && d.isNeighbor(i))
                counter++;
        }

        return counter;
    }

    private void updateCurrentCode(int i)
    {
        if (currentCode.length() < CODE_LENGTH - 1)
            currentCode += String.valueOf(i);
        else
            currentCode = currentCode.subSequence(0, CODE_LENGTH - 1) + String.valueOf(i);
    }

    public ArrayList<Long> getHints(int i)
    {
        state = GameState.ONGOING;

        final long IN = 1000;
        final long NEAR = 100;
        final long PAUSE = 50;

        updateCurrentCode(i);

        ArrayList<Long> result = new ArrayList<Long>();

        // If IN
        if (isInCode(i))
        {
            result.add(0l);
            result.add(IN);
        }

        // If has neighbors
        int nbNeighborCode = isNeighbor(i);
        while (nbNeighborCode-- > 0)
        {
            result.add(PAUSE);
            result.add(NEAR);
        }

        return result;
    }

    public boolean checkValidity()
    {
        if (code.equals(currentCode))
        {
            state = GameState.ENDED;
            return true;
        }

        state = GameState.FEEDBACK;
        currentCode = "";
        return false;
    }

    public void resetCurrentCode()
    {
        state = GameState.ONGOING;
        currentCode = "";
    }

    public String getCurrentCode()
    {
        return currentCode;
    }

    public GameState getState()
    {
        return state;
    }

    public Float getScore()
    {
        return score;
    }

    public Vector2 getWorldDimension()
    {
        return new Vector2(worldWidth, worldHeight);
    }
}
