package com.aenigma.games.touch;

import com.aenigma.games.GameScene;
import com.aenigma.games.touch.model.Target;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;

import java.util.ArrayList;

public class TouchView extends GameScene
{
    BitmapFont font;
    final Color BACKGROUND = Color.valueOf("1A2530FF");
    final Color MARKER = Color.valueOf("DF4949FF");
    final Color TARGET = Color.valueOf("EFC94C");
    final Color BAR = Color.valueOf("34495EFF");

    public TouchView()
    {
        // Generate font
        FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("fonts/AldotheApache.ttf"));
        FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameter.size = 100;
        font = generator.generateFont(parameter);
        generator.dispose();
    }

    public void render(TouchLogic logic)
    {
        // Fetch logic elements
        Vector2 leftBarPosition = logic.barLeft.getPosition();
        Vector2 leftBarSize = new Vector2(logic.barLeft.getSize().x, logic.barLeft.getSize().y);

        Vector2 rightBarPosition = logic.barRight.getPosition();
        Vector2 rightBarSize = new Vector2(logic.barRight.getSize().x, logic.barRight.getSize().y);

        Vector2 markerPosition = logic.marker.getPosition();
        Vector2 markerSize = logic.marker.getSize();

        ArrayList<Target> targets = logic.getTargets();

        // Set background color
        Gdx.gl.glClearColor(BACKGROUND.r, BACKGROUND.g, BACKGROUND.b, BACKGROUND.a);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        // Bind shapeRenderer to camera
        camera.update();
        shapeRenderer.setProjectionMatrix(camera.combined);

        // Display score and instructions
        spriteBatch.begin();
        float textHeight = font.getBounds("Score").height;
        font.draw(spriteBatch, "Score: " + (int) logic.getScore(), 100, 100 + textHeight);
        font.draw(spriteBatch, "Get rid of the targets!", 100, viewport.getWorldHeight() - 100);
        spriteBatch.end();

        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);

        // Display bars
        shapeRenderer.setColor(BAR);
        shapeRenderer.rect(leftBarPosition.x, leftBarPosition.y, leftBarSize.x, leftBarSize.y);
        shapeRenderer.rect(rightBarPosition.x, rightBarPosition.y, rightBarSize.x, rightBarSize.y);

        // Display targets
        for (Target f : targets)
        {
          shapeRenderer.setColor(TARGET);
          shapeRenderer.rect(f.getPosition().x, f.getPosition().y, f.getSize().x, f.getSize().y);
        }

        // Display marker
        shapeRenderer.setColor(MARKER);
        shapeRenderer.rect(markerPosition.x, markerPosition.y, markerSize.x, markerSize.y);

        shapeRenderer.end();
    }
}
