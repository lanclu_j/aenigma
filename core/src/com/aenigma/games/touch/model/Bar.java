package com.aenigma.games.touch.model;

import com.badlogic.gdx.math.Vector2;

public class Bar
{
    private Vector2 position;
    private float width;
    private float height;
    private int nbZones = 3;

    public Bar()
    {
        position = new Vector2(0, 0);
        width = 0;
        height = 0;
    }

    public Bar(Vector2 position, float width, float height)
    {
        this.position = position;
        this.width = width;
        this.height = height;
    }

    public void setPosition(float x, float y)
    {
        position.x = x;
        position.y = y;
    }

    public Vector2 getPosition()
    {
        return position;
    }

    public void setSize(float x, float y)
    {
        width = x;
        height = y;
    }

    public Vector2 getSize()
    {
        return new Vector2(width, height);
    }

    public int getNbZones()
    {
        return nbZones;
    }

    public void setNbZones(int value)
    {
       nbZones = value;
    }
}
