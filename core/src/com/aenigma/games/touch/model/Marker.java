package com.aenigma.games.touch.model;

import com.badlogic.gdx.math.Vector2;

public class Marker
{
    private Vector2 position;
    private float height;
    private float width;
    private float speed;
    private float maxSpeed;
    private Side direction;

    private Side currentBar;

    public Marker()
    {
        position = new Vector2(0, 0);
        currentBar = Side.LEFT;
        height = 0;
        width = 0;
    }

    public void update(Vector2 barPosition, float barWidth)
    {
        if (width <= 0)
            return;

        // Increasing speed and decreasing size
        if (Math.round(position.x) % 3 == 0)
        {
            width -= 0.5f;
            speed += 0.5f;
            if (speed >= maxSpeed)
                speed = maxSpeed;
        }

        // Handle left and right moves
        if (direction == Side.RIGHT)
        {
            if (position.x + width >= barPosition.x + barWidth)
                direction = Side.LEFT;
            else
                position.x += speed;
        }
        else if (direction == Side.LEFT)
        {
            if (position.x <= barPosition.x)
                direction = Side.RIGHT;
            else
                position.x -= speed;
        }
    }

    public void setSize(float width, float height)
    {
        this.width = width;
        this.height = height;
    }

    public Vector2 getSize()
    {
        return new Vector2(width, height);
    }

    public void setPosition(float x, float y)
    {
        position.x = x;
        position.y = y;
    }

    public Vector2 getPosition()
    {
        return position;
    }

    public void setDirection(Side s)
    {
        direction = s;
    }

    public void setSpeed(float speed)
    {
        this.speed = speed;
    }

    public void setMaxSpeed(float maxSpeed)
    {
        this.maxSpeed = maxSpeed;
    }
    public Side getCurrentBar()
    {
        return currentBar;
    }

    public void setCurrentBar(Side currentBar)
    {
        this.currentBar = currentBar;
    }

}
