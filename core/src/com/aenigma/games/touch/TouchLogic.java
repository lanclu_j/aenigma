package com.aenigma.games.touch;

import com.aenigma.games.GameState;
import com.aenigma.games.touch.model.Side;
import com.aenigma.games.touch.model.Bar;
import com.aenigma.games.touch.model.Marker;
import com.aenigma.games.touch.model.Target;
import com.badlogic.gdx.math.Vector2;

import java.util.ArrayList;

public class TouchLogic
{
    private GameState state;
    private Vector2 worldDimension;
    private float score;
    private float scoreOutOf;

    Bar barLeft;
    Bar barRight;
    Marker marker;
    ArrayList<Target> targets;
    float targetWidth = 1.0f;

    public TouchLogic()
    {
        state = GameState.INITIALIZING;
    }

    public void setGame()
    {
        setGame(worldDimension.x, worldDimension.y);
    }

    public void setGame(float worldWidth, float worldHeight)
    {
        score = 0;
        scoreOutOf = 1;
        worldDimension = new Vector2(worldWidth, worldHeight);
        float margin = 150;
        float height = 75;

        // Create left bar and set parameters
        barLeft = new Bar();
        barLeft.setSize(worldWidth - margin, height);
        barLeft.setPosition(-Math.round(worldWidth / 2) + (margin / 2),
                            -(worldHeight / 2) + (worldHeight / 3) - (height / 2));
        barLeft.setNbZones(3);

        // Create right bar and set parameters
        barRight = new Bar();
        barRight.setSize(worldWidth - margin, height);
        barRight.setPosition(-Math.round(worldWidth / 2) + (margin / 2),
                             -(worldHeight / 2) + 2 * (worldHeight / 3) - (height / 2));
        barRight.setNbZones(3);

        // Create marker and set parameters
        marker = new Marker();
        marker.setSize(Math.round((worldWidth - margin) / 4), height);
        marker.setPosition(Math.round(-(worldWidth / 2 + (margin / 2)) / 4), barLeft.getPosition().y);
        marker.setDirection(Side.LEFT);
        marker.setSpeed(0.5f);
        marker.setMaxSpeed(13f);

        // Generate targets on both bars
        targets = new ArrayList<Target>();
        targetWidth = barLeft.getSize().x / (barLeft.getNbZones() * 2);
        generateTargets(barLeft, targetWidth);
        generateTargets(barRight, targetWidth);
    }

    public void update()
    {
        marker.update(barLeft.getPosition(), barLeft.getSize().x);

        // If the game is over, do nothing.
        if (marker.getSize().x <= 0)
        {
            state = GameState.ENDED;
            return;
        }

        // Otherwise, update target size and position
        targetWidth -= 0.1f;

        for (int i = 0; i < targets.size(); i++)
        {
            Target target = targets.get(i);
            target.setSize(targetWidth);
            target.setPosition(target.getPosition().x + 0.05f, target.getPosition().y);
        }
    }

    public void generateTargets(Bar bar, float initialWidth)
    {
        // Create zones
        ArrayList<Float> zones = new ArrayList<Float>();
        int nbZones = bar.getNbZones();
        float width = bar.getSize().x;
        float height = bar.getSize().y;
        Vector2 barPos = bar.getPosition();

        for (int i = 0; i < nbZones; i++)
            zones.add(i, (width / nbZones) * i);

        // Create targets
        for (int i = 0; i < nbZones; i++)
        {
            float minBound = zones.get(i);
            float maxBound = minBound + width / nbZones - targetWidth;

            float relativePosition = ((float) Math.random()) * (maxBound - minBound);

            Target target = new Target();
            target.setSize(initialWidth, height);
            target.setPosition(barPos.x + (width / nbZones) * i + relativePosition, barPos.y);

            targets.add(i, target);
        }
    }

    public ArrayList<Target> getTargets()
    {
        return targets;
    }

    public GameState getState()
    {
        return state;
    }

    public float getScore()
    {
        return score;
    }

    public void updateScore()
    {
        score++;
    }

    public float getSuccessRate()
    {
        return score / scoreOutOf;
    }
}
