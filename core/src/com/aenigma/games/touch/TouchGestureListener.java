package com.aenigma.games.touch;

import com.aenigma.games.touch.model.Side;
import com.aenigma.games.touch.model.Target;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.math.Vector2;

public class TouchGestureListener implements GestureDetector.GestureListener
{
    private TouchLogic logic_;

    public TouchGestureListener(TouchLogic game)
    {
        logic_ = game;
    }

    @Override
    public boolean touchDown(float x, float y, int pointer, int button)
    {
        return false;
    }

    @Override
    public boolean tap(float x, float y, int count, int button)
    {
        // Dismiss targets if tapped
        for (int i = 0; i < logic_.targets.size(); i++)
        {
            Target target = logic_.targets.get(i);
            float targetMin = target.getPosition().x;
            float targetMax = targetMin + target.getSize().x;
            float markerMin = logic_.marker.getPosition().x;
            float markerMax = markerMin + logic_.marker.getSize().x;

            if (target.getPosition().y == logic_.marker.getPosition().y
                    && ((markerMin >= targetMin && markerMin <= targetMax)
                    || (markerMax >= targetMin && markerMax <= targetMax)
                    || (markerMin <= targetMin && markerMax >= targetMax)))
            {
                logic_.updateScore();
                logic_.targets.remove(i);
            }

            // Regenerate targets if none remaining
            if (logic_.targets.size() == 0)
            {
                logic_.generateTargets(logic_.barLeft, logic_.targetWidth);
                logic_.generateTargets(logic_.barRight, logic_.targetWidth);
            }
        }
        return false;
    }

    @Override
    public boolean longPress(float x, float y)
    {
        // Reset game
        logic_.setGame();
        return false;
    }

    @Override
    public boolean fling(float velocityX, float velocityY, int button)
    {
        // Moves marker from left to right
        if (logic_.marker.getCurrentBar() == Side.LEFT && velocityY < 0)
        {
            logic_.marker.setCurrentBar(Side.RIGHT);
            logic_.marker.getPosition().y = logic_.barRight.getPosition().y;
        }
        // Moves marker from right to left
        else if (logic_.marker.getCurrentBar() == Side.RIGHT && velocityY > 0)
        {
            logic_.marker.setCurrentBar(Side.LEFT);
            logic_.marker.getPosition().y = logic_.barLeft.getPosition().y;
        }

        return false;
    }

    @Override
    public boolean pan(float x, float y, float deltaX, float deltaY)
    {
        return false;
    }

    @Override
    public boolean panStop(float x, float y, int pointer, int button)
    {
        return false;
    }

    @Override
    public boolean zoom(float initialDistance, float distance)
    {
        return false;
    }

    @Override
    public boolean pinch(Vector2 initialPointer1, Vector2 initialPointer2, Vector2 pointer1, Vector2 pointer2)
    {
        return false;
    }
}
