package com.aenigma.games.touch;

import com.aenigma.games.GameAuditable;
import com.aenigma.games.GameScene;
import com.aenigma.games.GameState;
import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.input.GestureDetector;

import java.util.Date;
import java.util.Dictionary;

public class TouchGame extends ApplicationAdapter implements GameAuditable
{
    private TouchView touchView;
    private TouchLogic touchLogic;

    private Date startDate;
    private Date endDate;
    private Dictionary<String, Float> parameters;
    private Dictionary<String, Float> values;

    private GestureDetector detector;

    // ApplicationAdapater overrides

    @Override
    public void create()
    {
        // Initialize scene and logic
        touchLogic = new TouchLogic();
        touchView = new TouchView();

        // Attach sensor detector
        detector = new GestureDetector(new TouchGestureListener(touchLogic));
        Gdx.input.setInputProcessor(detector);

        // Start game
        startDate = new Date();
    }

    private void initializeParameters()
    {
       // FIXME
    }

    private void initializeValues()
    {
        // FIXME
    }

    @Override
    public void resize(int width, int height)
    {
        touchView.updateViewport(width, height);

        if (touchLogic.getState() == GameState.INITIALIZING)
            touchLogic.setGame(width, height);
    }

    @Override
    public void render()
    {
        touchLogic.update();
        touchView.render(touchLogic);

        if (touchLogic.getState() == GameState.ENDED)
        {
            endDate = new Date();
            Gdx.app.exit();
        }
    }

    // GameAuditable implementation

    @Override
    public Float getScore()
    {
        return touchLogic.getScore();
    }

    @Override
    public Float getSuccessRate()
    {
        return touchLogic.getSuccessRate();
    }

    @Override
    public Date getStartTime()
    {
        return startDate;
    }

    @Override
    public Date getEndTime()
    {
        return endDate;
    }

    @Override
    public Float getParameter(String name)
    {
        return parameters.get(name);
    }

    @Override
    public Float getValue(String name)
    {
        return values.get(name);
    }
}
