package com.aenigma.games.audio.model;

public class Timer
{
    private long startTime;

    public Timer(long maxTime)
    {
        startTime = -1;
    }

    public boolean hasStarted()
    {
        return startTime != -1;
    }

    public void start()
    {
        startTime = System.currentTimeMillis();
    }

    public long getElapsedTime()
    {
        if (startTime == -1)
            return 0;

        long now = System.currentTimeMillis();
        return now - startTime;
    }
}
