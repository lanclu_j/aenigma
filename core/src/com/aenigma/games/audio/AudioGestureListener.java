package com.aenigma.games.audio;

import com.aenigma.games.GameState;
import com.aenigma.games.audio.model.Disk;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.math.Vector2;

import java.util.ArrayList;

public class AudioGestureListener implements GestureDetector.GestureListener
{
    AudioLogic logic;
    ArrayList<Disk> disks;
    Texture repeat;
    Vector2 positionRepeat;

    public AudioGestureListener(AudioLogic logic)
    {
        this.logic = logic;
    }

    public void setDisks(ArrayList<Disk> disks)
    {
        this.disks = disks;
    }

    public void setRepeatPattern(Texture t, Vector2 position)
    {
        this.repeat = t;
        this.positionRepeat = position;
    }

    public boolean textureSet()
    {
        return repeat != null;
    }

    @Override
    public boolean touchDown(float x, float y, int pointer, int button)
    {
        return false;
    }

    @Override
    public boolean tap(float x, float y, int count, int button)
    {
        if (logic.getState() == GameState.ENDED)
            return false;

        float new_x = -logic.getWorldDimension().x / 2 + x;
        float new_y = logic.getWorldDimension().y / 2 - y;

        if (logic.getState() == GameState.ONGOING)
        {
            for (int i = 0; i < disks.size(); i++)
            {
                Disk d = disks.get(i);

                if ((d.getPosition().x <= new_x && new_x <= d.getPosition().x + d.getSize())
                        && (d.getPosition().y <= new_y && new_y <= d.getPosition().y + d.getSize()))
                {
                    if (i != 0)
                        logic.touchDisk(i);
                }
            }
        }

        if (positionRepeat != null)
        {
            if (positionRepeat.x <= new_x && new_x <= positionRepeat.x + repeat.getWidth()
                    && positionRepeat.y <= new_y && new_y <= positionRepeat.y + repeat.getHeight()
                    && !logic.getPlayer().isPlaying())
            {
                logic.getPlayer().playSequence();
            }
        }

        return false;
    }

    @Override
    public boolean longPress(float x, float y)
    {
        return false;
    }

    @Override
    public boolean fling(float velocityX, float velocityY, int button)
    {
        return false;
    }

    @Override
    public boolean pan(float x, float y, float deltaX, float deltaY)
    {
        return false;
    }

    @Override
    public boolean panStop(float x, float y, int pointer, int button)
    {
        return false;
    }

    @Override
    public boolean zoom(float initialDistance, float distance)
    {
        return false;
    }

    @Override
    public boolean pinch(Vector2 initialPointer1, Vector2 initialPointer2, Vector2 pointer1, Vector2 pointer2)
    {
        return false;
    }
}
