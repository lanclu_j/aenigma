package com.aenigma.games.audio;

import com.aenigma.games.GameAuditable;
import com.aenigma.games.GameState;
import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.input.GestureDetector;

import java.util.Date;
import java.util.Dictionary;

public class AudioGame extends ApplicationAdapter implements GameAuditable
{
    private AudioView audioView;
    private AudioLogic audioLogic;

    private Date startDate;
    private Date endDate;

    private Dictionary<String, Float> parameters;
    private Dictionary<String, Float> valus;

    private AudioGestureListener detector;

    @Override
    public void create()
    {
        audioView = new AudioView();
        audioLogic = new AudioLogic();

        detector = new AudioGestureListener(audioLogic);
        Gdx.input.setInputProcessor(new GestureDetector(detector));

        startDate = new Date();
    }

    @Override
    public void resize(int width, int height)
    {
        audioView.updateViewport(width, height);
        audioLogic.setGame(width, height);
        detector.setDisks(audioView.disks);
    }

    @Override
    public void render()
    {
        audioLogic.update();
        audioView.render(audioLogic);

        if (!detector.textureSet())
            detector.setRepeatPattern(audioView.getTexture(), audioView.getTexturePosition());

        if (audioLogic.getState() == GameState.ENDED)
        {
            endDate = new Date();
            audioLogic.getPlayer().disposeSound();
            Gdx.app.exit();
        }
    }

    @Override
    public Float getScore()
    {
        return null;
    }

    @Override
    public Float getSuccessRate()
    {
        return null;
    }

    @Override
    public Date getStartTime()
    {
        return null;
    }

    @Override
    public Date getEndTime()
    {
        return null;
    }

    @Override
    public Float getParameter(String name)
    {
        return null;
    }

    @Override
    public Float getValue(String name)
    {
        return null;
    }
}
